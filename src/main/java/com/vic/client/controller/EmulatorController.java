package com.vic.client.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Slf4j
@RestController
public class EmulatorController {

    private String dataDirectory;
    private String url1;
    private String url2;
    private RestTemplate restTemplate;
    private List<Path> files;
    private ExecutorService executor;

    public EmulatorController(@Value("${data-directory}") String dataDirectory,
                              @Value("${server-url-1}") String url1,
                              @Value("${server-url-2}") String url2,
                              @Value("${thread-pool-size}") int threadPoolSize,
                              @Autowired RestTemplate restTemplate) {
        this.dataDirectory = dataDirectory;
        this.url1 = url1;
        this.url2 = url2;
        this.restTemplate = restTemplate;
        executor = Executors.newFixedThreadPool(threadPoolSize);
    }

    @GetMapping("/start1")
    public String start1() throws IOException {
        files = scanDirectory(Paths.get(dataDirectory));
        if (CollectionUtils.isEmpty(files)) {
            log.warn("No files to send");
            return "OK";
        }
        sendFiles(file ->
                CompletableFuture.runAsync(() -> {
                    try {
                        sendFile1(file);
                    } catch (InterruptedException e) {
                        log.error("Can not send file", e);
                    }
                }, executor));
        return "OK";
    }

    private void sendFiles(Consumer<Path> sender) {
        for (Path file : files) {
            sender.accept(file);
        }
    }

    @GetMapping("/start2")
    public String start2() throws IOException {
        files = scanDirectory(Paths.get(dataDirectory));
        if (CollectionUtils.isEmpty(files)) {
            log.warn("No files to send");
            return "OK";
        }
        sendFiles(file ->
                CompletableFuture.runAsync(() -> {
                    try {
                        sendFile2(file);
                    } catch (Exception e) {
                        log.error("Can not send file", e);
                    }
                }, executor));
        return "OK";
    }

    private void sendFile2(Path file) throws InterruptedException, IOException {
        HttpHeaders headers = new HttpHeaders();
        //headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.add("Content-Type", "application/binary");
        byte[] bytes = Files.readAllBytes(file);
        HttpEntity<byte[]> requestEntity = new HttpEntity<>(bytes, headers);
        sendFile(file, url2, requestEntity);
    }

    private void sendFile(Path file, String url, HttpEntity<?> requestEntity) throws InterruptedException {
        ResponseEntity<String> response = null;
        boolean exitFlag;
        do {
            exitFlag = true;
            log.info("Send file: {}", file.getFileName());
            try {
                response = restTemplate.postForEntity(url, requestEntity, String.class);
            } catch (Exception e) {
                exitFlag = false;
                log.error("Can not send request", e);
                TimeUnit.MILLISECONDS.sleep(1000);
            }
        } while (!exitFlag);
        log.info("Response: {}", response.getBody());
    }

    private void sendFile1(Path file) throws InterruptedException {
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new FileSystemResource(file));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
        sendFile(file, url1, requestEntity);
    }

    private List<Path> scanDirectory(Path directory) throws IOException {
        log.debug("Directory for scan: {}", directory.toAbsolutePath());
        List<Path> result = Files.walk(directory).filter(file -> file.getFileName().toString().endsWith(".dat"))
                .collect(Collectors.toList());
        result.forEach(file -> log.debug("{}", file.getFileName()));
        return result;
    }

}
